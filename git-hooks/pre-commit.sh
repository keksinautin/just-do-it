#!/usr/bin/env bash

echo "The pre-commit script was run";

# Check if any .js file changed
git diff --cached --name-only --diff-filter=ACM | grep -iE '\.jsx?$' >/dev/null 2>&1

if [[ $? == 0 ]]; then
    node_modules/.bin/gulp pre-commit --gitHook
fi

exit $?
