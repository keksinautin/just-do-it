import Task from '../src/system/task';

describe('Task tests', function () {
	it('test compare', function () {
		const checks = [
			{
				args: [
					'startTime',
					true,
					Task.create({startTime: 5}),
					Task.create({startTime: 6})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'startTime',
					false,
					Task.create({startTime: 5}),
					Task.create({startTime: 6})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'startTime',
					true,
					Task.create({startTime: 7}),
					Task.create({startTime: 2})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'startTime',
					false,
					Task.create({startTime: 7}),
					Task.create({startTime: 2})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'startTime',
					true,
					Task.create({startTime: 7}),
					Task.create({startTime: 7})
				],
				checker(current) {
					expect(current).toEqual(0);
				}
			},
			{
				args: [
					'endTime',
					true,
					Task.create({endTime: 5}),
					Task.create({endTime: 6})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'endTime',
					true,
					Task.create({endTime: 7}),
					Task.create({endTime: 2})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'endTime',
					true,
					Task.create({endTime: 7}),
					Task.create({endTime: 7})
				],
				checker(current) {
					expect(current).toEqual(0);
				}
			},
			{
				args: [
					'cost',
					true,
					Task.create({cost: '55.00'}),
					Task.create({cost: '6.00'})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'cost',
					true,
					Task.create({cost: '5.51'}),
					Task.create({cost: '5.52'})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'cost',
					true,
					Task.create({cost: '5.52'}),
					Task.create({cost: '5.51'})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'cost',
					true,
					Task.create({cost: '5.32'}),
					Task.create({cost: '5.32'})
				],
				checker(current) {
					expect(current).toEqual(0);
				}
			},
			{
				args: [
					'name',
					true,
					Task.create({name: 'Bonnie'}),
					Task.create({name: 'Clyde'})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'name',
					true,
					Task.create({name: 'Clyde'}),
					Task.create({name: 'Bonnie'})
				],
				checker(current) {
					expect(current).toBeGreaterThan(0);
				}
			},
			{
				args: [
					'name',
					false,
					Task.create({name: 'Clyde'}),
					Task.create({name: 'Bonnie'})
				],
				checker(current) {
					expect(current).toBeLessThan(0);
				}
			},
			{
				args: [
					'name',
					true,
					Task.create({name: 'Clyde'}),
					Task.create({name: 'Clyde'})
				],
				checker(current) {
					expect(current).toEqual(0);
				}
			},
			{
				args: [
					'name',
					true,
					Task.create({name: 'Clyde'}),
					Task.create({name: 'clyde'})
				],
				checker(current) {
					expect(current).not.toEqual(0);
				}
			}
		];

		for (const check of checks) {
			check.checker(Task.compare(...check.args));
		}
	});
});
