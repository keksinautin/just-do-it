import {ACTIONS} from '../src/system/actions';
import TaskStorage from '../src/system/task-storage';
import Immutable from 'immutable';


describe('TaskStorage tests ->', function () {
	describe('load state ->', function () {
		describe('load active ->', function () {
			beforeEach(function () {
				this.expectedKey = 'JDI:ACTIVE';
				this.rawActive = {};
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					if (key === this.expectedKey) {
						return JSON.stringify(this.rawActive);
					}

					return null;
				});
			});

			it('full correct active', function () {
				this.rawActive = {
					name: 'name',
					cost: '0.20',
					startTime: 10,
					endTime: 20,
					id: '1'
				};

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				const state = storage.loadState();

				expect(Object.keys(state).length).toBe(1);
				expect(state.active instanceof Immutable.Map).toBeTruthy();
				expect(state.active.get('name')).toBe(this.rawActive.name);
				expect(state.active.get('cost')).toBe(this.rawActive.cost);
				expect(state.active.get('startTime')).toBe(this.rawActive.startTime);
				expect(state.active.get('endTime')).toBe(this.rawActive.endTime);
				expect(state.active.get('id')).toBe(this.rawActive.id);
			});

			it('empty active', function () {
				this.rawActive = {};

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				const state = storage.loadState();

				expect(Object.keys(state).length).toBe(1);
				expect(state.active instanceof Immutable.Map).toBeTruthy();
			});

			it('null active', function () {
				this.rawActive = null;

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				expect(() => {
					storage.loadState();
				}).toThrowError('The active task has an incorrect format');
			});

			it('active not found', function () {
				this.rawActive = null;
				this.engine = {getItem() {}};
				spyOn(this.engine, 'getItem').and.returnValue(null);

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);
				const state = storage.loadState();

				expect(Object.keys(state).length).toBe(0);
			});
		});

		describe('load tasks ->', function () {
			it('full correct tasks', function () {
				this.responses = {
					'JDI:TASKS': ['1', '2'],
					'JDI:TASK:1': {
						name: 'name',
						cost: '0.20',
						startTime: 10,
						endTime: 20,
						id: '1'
					},
					'JDI:TASK:2': {
						name: 'name2',
						cost: '0.40',
						startTime: 11,
						endTime: 22,
						id: '2'
					}
				};

				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					if (this.responses[key] != null) {
						return JSON.stringify(this.responses[key]);
					}

					return null;
				});

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);
				const state = storage.loadState();

				expect(Object.keys(state).length).toBe(1);
				expect(state.tasks instanceof Immutable.OrderedMap).toBeTruthy();
				expect(state.tasks.size).toBe(2);
				expect(state.tasks.get('1').get('name')).toBe('name');
				expect(state.tasks.get('2').get('name')).toBe('name2');
				expect(state.tasks.get('1').get('endTime')).toBe(20);
				expect(state.tasks.get('2').get('endTime')).toBe(22);
			});

			it('tasks not found', function () {
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.returnValue(null);

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);
				const state = storage.loadState();

				expect(Object.keys(state).length).toBe(0);
			});

			it('null tasks', function () {
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					if (key === 'JDI:TASKS') {
						return JSON.stringify(null);
					}

					return null;
				});

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				expect(() => {
					storage.loadState();
				}).toThrowError('The tasks field has an incorrect format');
			});

			it('task not found', function () {
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					if (key === 'JDI:TASKS') {
						return JSON.stringify(['1']);
					}

					return null;
				});

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				expect(() => {
					storage.loadState();
				}).toThrowError('The task was not found, id:1');
			});

			it('bad task found', function () {
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					switch (key) {
						case 'JDI:TASKS':
							return JSON.stringify(['1']);
						case 'JDI:TASK:1':
							return JSON.stringify(null);
					}

					return null;
				});

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				expect(() => {
					storage.loadState();
				}).toThrowError('The task has an incorrect format, id:1');
			});

			it('empty tasks list', function () {
				this.engine = {getItem() {}};

				spyOn(this.engine, 'getItem').and.callFake((key) => {
					if (key === 'JDI:TASKS') {
						return JSON.stringify([]);
					}

					return null;
				});

				//noinspection JSCheckFunctionSignatures
				const storage = new TaskStorage(this.engine);

				expect(Object.keys(storage.loadState()).length).toBe(0);
			});
		});
	});

	describe('save sate ->', function () {
		it('save active', function () {
			const engine = {setItem() {}};
			spyOn(engine, 'setItem');

			const store = {
				getState() {
					return {
						active: Immutable.Map({id: '2', name: 'name', startTime: 10})
					};
				}
			};
			const nextDispatch = jasmine.createSpy('nextDispatch');
			const action = {type: ACTIONS.SET_ACTIVE};

			//noinspection JSCheckFunctionSignatures
			const storage = new TaskStorage(engine);
			const dispatcher = storage.getSaver()(store)(nextDispatch);
			dispatcher(action);


			expect(engine.setItem).toHaveBeenCalledWith('JDI:ACTIVE', '{"id":"2","name":"name","startTime":10}');
			expect(nextDispatch).toHaveBeenCalledWith(action);
		});

		it('save task', function () {
			const engine = {setItem() {}};
			spyOn(engine, 'setItem');

			const store = {
				getState() {
					return {
						tasks: Immutable.OrderedMap([['1', Immutable.Map({id: '1', startTime: 20})]])
					};
				}
			};
			const nextDispatch = jasmine.createSpy('nextDispatch');
			const action = {
				type: ACTIONS.SET_TASK,
				payload: Immutable.Map({id: '1'})
			};

			//noinspection JSCheckFunctionSignatures
			const storage = new TaskStorage(engine);
			const dispatcher = storage.getSaver()(store)(nextDispatch);
			dispatcher(action);


			expect(engine.setItem.calls.argsFor(0)).toEqual(['JDI:TASK:1', '{"id":"1","startTime":20}']);
			expect(engine.setItem.calls.argsFor(1)).toEqual(['JDI:TASKS', '["1"]']);
			expect(nextDispatch).toHaveBeenCalledWith(action);
		});

		it('remove task', function () {
			const engine = {removeItem() {}, setItem() {}};
			spyOn(engine, 'removeItem');
			spyOn(engine, 'setItem');

			const store = {
				getState() {
					return {
						tasks: Immutable.OrderedMap([['1', Immutable.Map({id: '1', startTime: 20})]])
					};
				}
			};
			const nextDispatch = jasmine.createSpy('nextDispatch');
			const action = {
				type: ACTIONS.REMOVE_TASK,
				payload: Immutable.Map({id: '2'})
			};

			//noinspection JSCheckFunctionSignatures
			const storage = new TaskStorage(engine);
			const dispatcher = storage.getSaver()(store)(nextDispatch);
			dispatcher(action);


			expect(engine.removeItem).toHaveBeenCalledWith('JDI:TASK:2');
			expect(engine.setItem).toHaveBeenCalledWith('JDI:TASKS', '["1"]');
			expect(nextDispatch).toHaveBeenCalledWith(action);
		});
	});
});
