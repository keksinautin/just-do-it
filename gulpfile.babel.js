import {argv} from 'optimist';
import gulp from 'gulp';
import runSequence from 'run-sequence';
import requireDir from 'require-dir';

requireDir(`${__dirname}/gulp/tasks`, {recurse: true});


gulp.task('build', (done) => {
	const procedures = [
		[
			'scripts',
			'resources',
			'templates',
			'styles'
		],
		done
	];

	if (argv.setGitHooks) {
		procedures[0].push('set-git-hooks');
	}

	if (!argv.debug) {
		procedures.unshift('clear');
	}

	return runSequence(...procedures);
});


gulp.task('pre-commit', ['lint', 'test']);


gulp.task('start', ['build'], (done) => {
	return runSequence(
		'server',
		'watch',
		done
	);
});


gulp.task('default', ['build']);
