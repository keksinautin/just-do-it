import 'babel-polyfill';
import App from './widgets/app';
import appReducer from './system/app-reducer';
import jQuery from 'jquery';
import {Provider} from 'react-redux';
import React from 'react';
import ReactDom from 'react-dom';
import * as Redux from 'redux';
import TaskStorage from './system/task-storage';


jQuery(document).ready(() => {
	const storage = new TaskStorage();

	const store = Redux.applyMiddleware(storage.getSaver())(Redux.createStore)(
		appReducer(), storage.loadState()
	);

	ReactDom.render(
		<Provider store={store}>
			<App />
		</Provider>,
		document.body.appendChild(document.createElement('div'))
	);
});
