import 'browsernizr/test/inputtypes';
import 'jquery-ui/datepicker';
import 'jquery-ui-timepicker-addon';
import jQuery from 'jquery';
import modernizr from 'browsernizr';
import moment from 'moment';
import moneyMath from 'money-math';
import React from 'react';


export default class FormCustom extends React.Component {
	componentDidMount() {
		if (!modernizr.inputtypes.date) {
			jQuery(this.refs.date).datepicker({dateFormat: 'yy-mm-dd'});
			jQuery(this.refs.startTime).timepicker();
			jQuery(this.refs.endTime).timepicker();
		}
	}


	componentWillUnmount() {
		if (!modernizr.inputtypes.date) {
			jQuery(this.refs.date).datepicker('destroy');
			jQuery(this.refs.startTime).timepicker('destroy');
			jQuery(this.refs.endTime).timepicker('destroy');
		}
	}


	submit(e) {
		e.stopPropagation();
		e.preventDefault();

		const name = this.refs.name.value;
		const cost = moneyMath.floatToAmount(Number(this.refs.cost.value) || 0);
		const startTime = moment(`${this.refs.date.value} ${this.refs.startTime.value}`).valueOf();
		const endTime = moment(`${this.refs.date.value} ${this.refs.endTime.value}`).valueOf();

		this.props.onSubmit({name, cost, startTime, endTime});
	}


	render() {
		return (
			<form className="row" onSubmit={this.submit.bind(this)}>
				<div className="small-12 columns">
					<div className="row collapse">
						<div className="small-12 large-4 columns">
							<input type="text" placeholder="Enter a task name" ref="name"/>
						</div>
						<div className="small-6 large-2 columns">
							<input type="time" defaultValue={moment().format('HH:mm')} ref="startTime"/>
							<span className="round label jdi-form-label">start</span>
						</div>
						<div className="small-6 large-2 columns">
							<input type="time" defaultValue={moment().format('HH:mm')} ref="endTime"/>
							<span className="round label jdi-form-label">finish</span>
						</div>
						<div className="small-6 large-2 columns">
							<input type="date" defaultValue={moment().format('YYYY-MM-DD')} ref="date"/>
							<span className="round label jdi-form-label">date</span>
						</div>
						<div className="small-3 large-1 columns">
							<input
								type="number"
								placeholder="$"
								step="0.01"
								min="0"
								max={Number.MAX_SAFE_INTEGER / 100}
								defaultValue="0.00"
								ref="cost"/>
							<span className="round label jdi-form-label">cost</span>
						</div>
						<div className="small-3 large-1 columns">
							<button
								role="button"
								className="button postfix jdi-btn">Just Did It</button>
						</div>
					</div>
				</div>
			</form>
		);
	}


	static get propTypes() {
		return {
			onSubmit: React.PropTypes.func
		};
	}
}
