import React from 'react';


export default class Switcher extends React.Component {
	render() {
		return (
			<ul className="button-group round">
				{(() => {
					return this.props.states.map((stateName) => {
						const modifier = stateName === this.props.active ? '' : 'secondary';

						return (
							<li className="small-6" key={stateName}>
								<a
									href="javascript:;"
									role="button"
									onClick={this.props.onChange.bind(null, stateName)}
									className={`button postfix tiny jdi-switcher ${modifier}`}>{stateName}</a>
							</li>
						);
					});
				})()}
			</ul>
		);
	}


	static get propTypes() {
		return {
			states: React.PropTypes.arrayOf(React.PropTypes.string),
			active: React.PropTypes.string,
			onChange: React.PropTypes.func
		};
	}
}
