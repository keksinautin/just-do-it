import ChangeableFieldCost from './changeable-field-cost';
import ChangeableFieldDate from './changeable-field-date';
import ChangeableFieldSimple from './changeable-field-simple';
import ChangeableFieldTime from './changeable-field-time';
import Immutable from 'immutable';
import moment from 'moment';
import moneyMath from 'money-math';
import React from 'react';


class TasksTable extends React.Component {
	/**
	 * @param task {Task}
	 * @param e {SyntheticUIEvent|Event|null}
     */
	onRemove(task, e) {
		if (e) {
			e.preventDefault();
			e.stopPropagation();
		}

		this.props.onRemove(task);
	}


	/**
	 * @param field {string}
	 * @param addClass {string}
	 * @returns {XML}
     */
	renderSorting(field, addClass = '') {
		const classes = ['jdi-icon', 'jdi-icon-sorting'];
		classes.push(addClass);

		if (this.props.sorting.get('directOrder')) {
			classes.push('fi-arrow-down');
		} else {
			classes.push('fi-arrow-up');
		}

		let newOrder = this.props.sorting.get('directOrder');
		if (field === this.props.sorting.get('field')) {
			classes.push('jdi-icon-sorting_active');
			newOrder = !newOrder;
		}

		const change = (e) => {
			if (e) {
				e.preventDefault();
				e.stopPropagation();
			}

			this.props.onResort(
				this.props.sorting.set('field', field).set('directOrder', newOrder)
			);
		};

		return (
			<i title="sort" className={classes.join(' ')} onClick={change} />
		);
	}


	/**
	 * @param task {Task}
	 * @returns {XML}
	 */
	renderTask(task) {
		const duration = moment
			.duration(task.get('endTime') - task.get('startTime'))
			.format('H[h] m[m] s[s]', {forceLength: true});
		const hours = (task.get('endTime') - task.get('startTime')) / (60 * 60 * 1000);
		const totalCost = moneyMath.mul(
			moneyMath.floatToAmount(hours), task.get('cost')
		);

		return (
			<tr key={task.get('id')}>
				<td className="row small-collapse jdi-task-str">
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Name:
						{this.renderSorting('name', 'jdi-icon-sorting_small-screen')}
					</div>
					<ChangeableFieldSimple
						addClass="small-8 medium-6 large-2 columns jdi-task-ceil_name"
						field="name"
						onChange={this.props.onChange}
						placeholder="Enter a task name"
						task={task}
						title={task.get('name')}
						type="text"
					>
						<i
							onClick={this.onRemove.bind(this, task)}
							title="Remove task"
							className="fi-x jdi-icon jdi-icon-remove"/>
					</ChangeableFieldSimple>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Start:
						{this.renderSorting('startTime', 'jdi-icon-sorting_small-screen')}
					</div>
					<ChangeableFieldTime
						task={task}
						addClass="small-8 medium-3 large-2 columns"
						title="Start Time"
						field="startTime"
						date={task.get('startTime')}
						onChange={this.props.onChange}
					/>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						End:
						{this.renderSorting('endTime', 'jdi-icon-sorting_small-screen')}
					</div>
					<ChangeableFieldTime
						task={task}
						addClass="small-8 medium-3 large-2 columns"
						title="End Time"
						field="endTime"
						date={task.get('startTime')}
						onChange={this.props.onChange}
					/>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Date:
					</div>
					<ChangeableFieldDate
						task={task}
						addClass="small-8 medium-3 large-2 columns"
						title="Date"
						onChange={this.props.onChange}
					/>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Duration:
					</div>
					<div
						title="Duration"
						className="small-8 medium-3 large-2 columns jdi-task-ceil">
						{duration}
					</div>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Cost:
						{this.renderSorting('cost', 'jdi-icon-sorting_small-screen')}
					</div>
					<ChangeableFieldCost
						addClass="small-8 medium-3 large-1 columns"
						field="cost"
						onChange={this.props.onChange}
						task={task}
						title="Cost"
					/>
					<div className="small-4 show-for-small-only columns jdi-task-ceil jdi-task-ceil_label">
						Total:
					</div>
					<div
						title="Total cost"
						className="small-8 medium-3 large-1 columns jdi-task-ceil">
						{totalCost}
					</div>
				</td>
			</tr>
		);
	}


	render() {
		return (
			<table className="small-12">
				<thead>
				<tr>
					<th className="jdi-task-str">
						<div className="small-6 large-2 columns hide-for-small-only jdi-task-ceil jdi-task-ceil_name">
							Name
							{this.renderSorting('name', 'jdi-icon-sorting_big-screen')}
						</div>
						<div className="small-3 large-2 columns hide-for-small-only jdi-task-ceil">
							Start
							{this.renderSorting('startTime', 'jdi-icon-sorting_big-screen')}
						</div>
						<div className="small-3 large-2 columns hide-for-small-only jdi-task-ceil">
							End
							{this.renderSorting('endTime', 'jdi-icon-sorting_big-screen')}
						</div>
						<div className="small-3 large-2 columns hide-for-small-only jdi-task-ceil">
							Date
						</div>
						<div className="small-3 large-2 columns hide-for-small-only jdi-task-ceil">Duration</div>
						<div className="small-3 large-1 columns hide-for-small-only jdi-task-ceil">
							Cost
							{this.renderSorting('cost', 'jdi-icon-sorting_big-screen')}
						</div>
						<div className="small-3 large-1 columns hide-for-small-only jdi-task-ceil">Total</div>
					</th>
				</tr>
				</thead>
				<tbody>
					{(() => this.props.tasks.map(this.renderTask.bind(this)).toArray())()}
				</tbody>
			</table>
		);
	}


	static get propTypes() {
		return {
			tasks: React.PropTypes.instanceOf(Immutable.OrderedMap),
			onChange: React.PropTypes.func,
			onRemove: React.PropTypes.func,
			onResort: React.PropTypes.func,
			sorting: React.PropTypes.instanceOf(Immutable.Map)
		};
	}
}


export default TasksTable;
