import ChangeableField from './changeable-field';
import Immutable from 'immutable';
import jQuery from 'jquery';
import modernizr from 'browsernizr';
import moment from 'moment';
import React from 'react';


class ChangeableFieldTime extends ChangeableField {
	// it is necessarily for work in <=ie10 (babel use __proto__)
	constructor(...args) {
		super(...args);
		this.state = {
			changing: false
		};
	}


	componentDidUpdate() {
		if (!modernizr.inputtypes.date && this.refs.input) {
			const $tp = jQuery(this.refs.input).timepicker({
				onClose: () => {
					this.toggleChanging();
					$tp.timepicker('destroy');
				}
			});
		}
	}


	onBlur(e) {
		if (modernizr.inputtypes.date) {
			this.toggleChanging(e);
		}
	}


	toggleChanging(...args) {
		if (this.refs.input) {
			const date = moment(this.props.date).format('YYYY-MM-DD');
			const newTime = moment(`${date} ${this.refs.input.value}`).valueOf();
			this.props.onChange(this.props.task.set(this.props.field, newTime));

		} else {
			setTimeout(() => {
				if (this.refs.input) {
					this.refs.input.focus();
				}
			}, 0);
		}

		super.toggleChanging(...args);
	}


	renderChanging() {
		return (
			<div className={`${this.props.addClass} jdi-task-ceil jdi-task-ceil_input`}>
				<input
					type="time"
					className="jdi-task-input"
					defaultValue={moment(this.props.task.get(this.props.field)).format('HH:mm')}
					onBlur={this.onBlur.bind(this)}
					ref="input"/>
			</div>
		);
	}


	renderStatic() {
		return (
			<div
				title={this.props.title}
				onClick={this.toggleChanging.bind(this)}
				className={`${this.props.addClass} jdi-task-ceil`}>
				{moment(this.props.task.get(this.props.field)).format('HH:mm')}
				<i title="changeable" className="fi-pencil jdi-icon-changeable"/>
			</div>
		);
	}


	static get propTypes() {
		return {
			addClass: React.PropTypes.string,
			date: React.PropTypes.number,
			field: React.PropTypes.string,
			onChange: React.PropTypes.func,
			task: React.PropTypes.instanceOf(Immutable.Map),
			title: React.PropTypes.string
		};
	}
}


export default ChangeableFieldTime;
