import ChangeableField from './changeable-field';
import Immutable from 'immutable';
import React from 'react';


class ChangeableFieldSimple extends ChangeableField {
	// it is necessarily for work in <=ie10 (babel use __proto__)
	constructor(...args) {
		super(...args);
		this.state = {
			changing: false
		};
	}


	toggleChanging(...args) {
		if (this.refs.input) {
			this.props.onChange(this.props.task.set(this.props.field, this.refs.input.value));

		} else {
			setTimeout(() => {
				if (this.refs.input) {
					this.refs.input.focus();
				}
			}, 0);
		}

		super.toggleChanging(...args);
	}


	renderChanging() {
		return (
			<div className={`${this.props.addClass} jdi-task-ceil jdi-task-ceil_input`}>
				<input
					type={this.props.type}
					className="jdi-task-input"
					placeholder={this.props.placeholder}
					defaultValue={this.props.task.get(this.props.field)}
					onBlur={this.toggleChanging.bind(this)}
					ref="input"/>
			</div>
		);
	}


	renderStatic() {
		return (
			<div
				onClick={this.toggleChanging.bind(this)}
				title={this.props.title}
				className={`${this.props.addClass} jdi-task-ceil`}>
				{this.props.children}
				<span>{this.props.task.get(this.props.field)}</span>
				<i title="changeable" className="fi-pencil jdi-icon-changeable"/>
			</div>
		);
	}


	static get defaultProps() {
		return {
			addClass: '',
			onChange: () => {},
			placeholder: '',
			title: '',
			type: 'text'
		};
	}


	static get propTypes() {
		return {
			addClass: React.PropTypes.string,
			field: React.PropTypes.string,
			onChange: React.PropTypes.func,
			placeholder: React.PropTypes.string,
			task: React.PropTypes.instanceOf(Immutable.Map),
			title: React.PropTypes.string,
			type: React.PropTypes.string
		};
	}
}


export default ChangeableFieldSimple;
