import ChangeableFieldSimple from './changeable-field-simple';
import Immutable from 'immutable';
import jQuery from 'jquery';
import moneyMath from 'money-math';
import React from 'react';


class ChangeableFieldCost extends ChangeableFieldSimple {
	// it is necessarily for work in <=ie10 (babel use __proto__)
	constructor(...args) {
		super(...args);
		this.state = {
			changing: false
		};
	}


	toggleChanging(...args) {
		if (this.refs.input) {
			this.refs.input.value = moneyMath.floatToAmount(this.refs.input.value);
		}

		super.toggleChanging(...args);
	}


	renderChanging() {
		return (
			<div className={`${this.props.addClass} jdi-task-ceil jdi-task-ceil_input`}>
				<input
					type={this.props.type}
					className="jdi-task-input"
					placeholder={this.props.placeholder}
					step="0.01"
					min="0"
					max={Number.MAX_SAFE_INTEGER / 100}
					defaultValue={this.props.task.get(this.props.field)}
					onFocus={() => {
						jQuery(this.refs.input).blur(this.toggleChanging.bind(this));
					}}
					ref="input"/>
			</div>
		);
	}


	static get defaultProps() {
		return {
			addClass: '',
			onChange: () => {},
			placeholder: '$',
			title: '',
			type: 'number'
		};
	}


	static get propTypes() {
		return {
			addClass: React.PropTypes.string,
			field: React.PropTypes.string,
			onChange: React.PropTypes.func,
			placeholder: React.PropTypes.string,
			task: React.PropTypes.instanceOf(Immutable.Map),
			title: React.PropTypes.string,
			type: React.PropTypes.string
		};
	}
}


export default ChangeableFieldCost;
