import ChangeableField from './changeable-field';
import Immutable from 'immutable';
import jQuery from 'jquery';
import modernizr from 'browsernizr';
import moment from 'moment';
import React from 'react';


class ChangeableFieldDate extends ChangeableField {
	// it is necessarily for work in <=ie10 (babel use __proto__)
	constructor(...args) {
		super(...args);
		this.state = {
			changing: false
		};
	}


	componentDidUpdate() {
		if (!modernizr.inputtypes.date && this.refs.input) {
			const $dp = jQuery(this.refs.input).datepicker({
				dateFormat: 'yy-mm-dd',
				onClose: () => {
					this.toggleChanging();
					$dp.datepicker('destroy');
				}
			});
		}
	}


	onBlur(e) {
		if (modernizr.inputtypes.date) {
			this.toggleChanging(e);
		}
	}


	toggleChanging(...args) {
		if (this.refs.input) {
			const startTimeStr = moment(this.props.task.get('startTime')).format('HH:mm');
			const endTimeStr = moment(this.props.task.get('endTime')).format('HH:mm');

			const startTime = moment(`${this.refs.input.value} ${startTimeStr}`).valueOf();
			const endTime = moment(`${this.refs.input.value} ${endTimeStr}`).valueOf();

			this.props.onChange(
				this.props.task.set('startTime', startTime).set('endTime', endTime)
			);

		} else {
			setTimeout(() => {
				if (this.refs.input) {
					this.refs.input.focus();
				}
			}, 0);
		}

		super.toggleChanging(...args);
	}


	renderChanging() {
		return (
			<div className={`${this.props.addClass} jdi-task-ceil jdi-task-ceil_input`}>
				<input
					type="date"
					className="jdi-task-input"
					defaultValue={moment(this.props.task.get('startTime')).format('YYYY-MM-DD')}
					onBlur={this.onBlur.bind(this)}
					ref="input"/>
			</div>
		);
	}


	renderStatic() {
		return (
			<div
				title={this.props.title}
				onClick={this.toggleChanging.bind(this)}
				className={`${this.props.addClass} jdi-task-ceil`}>
				{moment(this.props.task.get('startTime')).format('YYYY-MM-DD')}
				<i title="changeable" className="fi-pencil jdi-icon-changeable"/>
			</div>
		);
	}


	static get propTypes() {
		return {
			addClass: React.PropTypes.string,
			onChange: React.PropTypes.func,
			task: React.PropTypes.instanceOf(Immutable.Map),
			title: React.PropTypes.string
		};
	}
}


export default ChangeableFieldDate;
