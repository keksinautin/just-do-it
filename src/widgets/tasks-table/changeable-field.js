import React from 'react';


class ChangeableField extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			changing: false
		};
	}


	/**
	 * @param e {SyntheticUIEvent|Event|null}
	 */
	toggleChanging(e = null) {
		if (e) {
			e.preventDefault();
			e.stopPropagation();
		}

		this.setState({changing: !this.state.changing});
	}


	/**
	 * @abstract
	 * @returns {XML}
	 */
	renderChanging() {}


	/**
	 * @abstract
	 * @returns {XML}
	 */
	renderStatic() {}


	render() {
		if (this.state.changing) {
			return this.renderChanging();
		}

		return this.renderStatic();
	}
}


export default ChangeableField;
