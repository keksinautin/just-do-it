import 'moment-duration-format';
import Immutable from 'immutable';
import moment from 'moment';
import moneyMath from 'money-math';
import React from 'react';
import Task from '../system/task';


class FormTimer extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			duration: 0
		};
		this.updateDurationTimerID = null;
	}


	componentDidMount() {
		this.updateDuration();
	}


	componentWillReceiveProps(nextProps) {
		this.updateDuration(nextProps);
	}


	componentWillUnmount() {
		clearTimeout(this.updateDurationTimerID);
		this.updateDurationTimerID = null;
	}


	/**
	 * @param fieldName {string}
     */
	onBlur(fieldName) {
		if (this.props.activeTask.get('startTime')) {
			let newTask = this.props.activeTask;

			switch (fieldName) {
				case 'name':
					newTask = newTask.set('name', this.refs.name.value);
					break;
				case 'cost':
					newTask = newTask.set('cost', moneyMath.floatToAmount(this.refs.cost.value));
					break;
			}

			if (!newTask.equals(this.props.activeTask)) {
				this.props.onChange(newTask);
			}
		}
	}


	updateDuration(props = this.props) {
		if (this.updateDurationTimerID) {
			clearTimeout(this.updateDurationTimerID);
			this.updateDurationTimerID = null;
		}

		if (props.activeTask.get('startTime')) {
			this.setState({duration: Math.ceil((Date.now() - props.activeTask.get('startTime')) / 1000)});
			this.updateDurationTimerID = setTimeout(this.updateDuration.bind(this), 1000);

		} else {
			this.setState({duration: 0});

		}
	}


	/**
	 * @param e {SyntheticUIEvent|Event|null}
     */
	submit(e) {
		if (e) {
			e.stopPropagation();
			e.preventDefault();
		}

		if (this.props.activeTask.get('startTime')) {
			this.props.onStop(
				this.props.activeTask.set('endTime', Date.now()),
				Task.create({})
			);

		} else {
			this.props.onStart(
				this.props.activeTask
					.set('name', this.refs.name.value)
					.set('cost', moneyMath.floatToAmount(Number(this.refs.cost.value) || 0))
					.set('startTime', Date.now())
			);
		}
	}


	render() {
		const duration = moment.duration(this.state.duration, 'seconds')
			.format('H[h] m[m] s[s]', {forceLength: true});

		const btnClass = this.props.activeTask.get('startTime') ? 'alert' : 'success';
		const btnText = this.props.activeTask.get('startTime') ? 'Stop' : 'Just Do It';

		return (
			<form className="row" onSubmit={this.submit.bind(this)}>
				<div className="small-12 columns">
					<div className="row collapse">
						<div className="small-12 medium-12 large-8 columns">
							<input
								type="text"
								placeholder="Enter a task name"
								defaultValue={this.props.activeTask.get('name')}
								onBlur={this.onBlur.bind(this, 'name')}
								ref="name"/>
						</div>
						<div className="small-6 medium-4 large-2 columns">
							<input
								type="text"
								ref="duration"
								value={duration}
								readOnly/>
						</div>
						<div className="small-3 medium-4 large-1 columns">
							<input
								type="number"
								placeholder="$"
								step="0.01"
								defaultValue={this.props.activeTask.get('cost')}
								min="0"
								max={Number.MAX_SAFE_INTEGER / 100}
								onBlur={this.onBlur.bind(this, 'cost')}
								ref="cost"/>
							<span className="round label jdi-form-label">cost</span>
						</div>
						<div className="small-3 medium-4 large-1 columns">
							<button
								type="submit"
								className={`button postfix ${btnClass} jdi-btn`}>{btnText}</button>
						</div>
					</div>
				</div>
			</form>
		);
	}


	static get propTypes() {
		return {
			activeTask: React.PropTypes.instanceOf(Immutable.Map),
			onChange: React.PropTypes.func,
			onStart: React.PropTypes.func,
			onStop: React.PropTypes.func
		};
	}
}


export default FormTimer;
