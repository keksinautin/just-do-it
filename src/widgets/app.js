import {ACTIONS, factory as actionsFactory} from '../system/actions';
import {connect} from 'react-redux';
import FormCustom from './form-custom';
import FormTimer from './form-timer';
import Immutable from 'immutable';
import React from 'react';
import Switcher from './switcher';
import Task from '../system/task';
import TasksTable from './tasks-table/tasks-table';

const FORM_STATES = {TIMER: 'timer', CUSTOM: 'custom'};
Object.freeze(FORM_STATES);


class App extends React.Component {
	constructor(props, ...args) {
		super(props, ...args);
		this.state = {
			form: this.constructor.FORM_STATES.TIMER
		};
	}


	addNewTask(task) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_TASK](Task.create(task)));
	}


	changeActive(task) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_ACTIVE](task));
	}


	changeForm(formState) {
		this.setState({form: formState});
	}


	changeSorting(sorting) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_SORTING](sorting));
	}


	changeTask(task) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_TASK](task));
	}


	removeTask(task) {
		this.props.dispatch(actionsFactory[ACTIONS.REMOVE_TASK](task));
	}


	startTimer(task) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_ACTIVE](task));
	}


	stopTimer(stoppedTask, nextTask) {
		this.props.dispatch(actionsFactory[ACTIONS.SET_TASK](stoppedTask));
		this.props.dispatch(actionsFactory[ACTIONS.SET_ACTIVE](nextTask));
	}


	render() {
		return (
			<div className="jdi-app">
				<div className="row">
					<div className="small-12 medium-3 medium-offset-9 large-2 large-offset-10 columns">
						<Switcher
							states={[this.constructor.FORM_STATES.TIMER, this.constructor.FORM_STATES.CUSTOM]}
							active={this.state.form}
							onChange={this.changeForm.bind(this)}
						/>
					</div>
				</div>
				{(() => {
					switch (this.state.form) {
						case this.constructor.FORM_STATES.TIMER:
							return (
								<FormTimer
									onChange={this.changeActive.bind(this)}
									onStart={this.startTimer.bind(this)}
									onStop={this.stopTimer.bind(this)}
									activeTask={this.props.active}/>
							);
						case this.constructor.FORM_STATES.CUSTOM:
							return (
								<FormCustom onSubmit={this.addNewTask.bind(this)}/>
							);
					}
				})()}
				<div className="row">
					<div className="small-12 columns">
						<TasksTable
							tasks={this.props.tasks}
							onChange={this.changeTask.bind(this)}
							onRemove={this.removeTask.bind(this)}
							onResort={this.changeSorting.bind(this)}
							sorting={this.props.sorting}
						/>
					</div>
				</div>
			</div>
		);
	}


	/**
	 * @returns {{TIMER: string, CUSTOM: string}}
	 * @constructor
     */
	static get FORM_STATES() {
		return FORM_STATES;
	}


	static get propTypes() {
		return {
			active: React.PropTypes.instanceOf(Immutable.Map),
			dispatch: React.PropTypes.func,
			tasks: React.PropTypes.instanceOf(Immutable.OrderedMap),
			sorting: React.PropTypes.instanceOf(Immutable.Map)
		};
	}
}


export default connect((state) => state)(App);
