import Immutable from 'immutable';

/**
 * @typedef {Immutable.Map}
 */
export default class Sorting {
	/**
	 * @param field {string}
	 * @param directOrder {boolean}
	 * @returns {Sorting}
	 */
	static create({field = 'startTime', directOrder = false}) {
		directOrder = Boolean(directOrder);
		return Immutable.Map({field, directOrder});
	}
}
