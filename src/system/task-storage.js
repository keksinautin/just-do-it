import {ACTIONS} from './actions';
import Immutable from 'immutable';
import Task from './task';
import Sorting from './sorting';

const _engine = Symbol('_engine');
const APP_PREFIX = 'JDI';
const KEYS = {
	ACTIVE: `${APP_PREFIX}:ACTIVE`,
	TASK: `${APP_PREFIX}:TASK`,
	TASKS: `${APP_PREFIX}:TASKS`,
	SORTING: `${APP_PREFIX}:SORTING`,
	task(id) {
		return `${this.TASK}:${id}`;
	}
};

export default class TaskStorage {
	/**
	 * @param engine {Storage}
     */
	constructor(engine = localStorage) {
		this[_engine] = engine;
	}


	/**
	 * @returns {Function}
     */
	getSaver() {
		return (store) => (nextDispatch) => (action) => {
			const result = nextDispatch(action);

			switch (action.type) {
				case ACTIONS.REMOVE_TASK:
					{
						const id = action.payload.get('id');
						this[_engine].removeItem(KEYS.task(id));
						this[_engine].setItem(KEYS.TASKS, JSON.stringify(store.getState().tasks.keySeq().toJS()));
					}
					break;

				case ACTIONS.SET_ACTIVE:
					{
						this[_engine].setItem(KEYS.ACTIVE, JSON.stringify(store.getState().active.toJS()));
					}
					break;

				case ACTIONS.SET_SORTING:
					{
						this[_engine].setItem(KEYS.SORTING, JSON.stringify(store.getState().sorting.toJS()));
						this[_engine].setItem(KEYS.TASKS, JSON.stringify(store.getState().tasks.keySeq().toJS()));
					}
					break;

				case ACTIONS.SET_TASK:
					{
						const id = action.payload.get('id');
						const task = store.getState().tasks.get(id);
						this[_engine].setItem(KEYS.task(id), JSON.stringify(task.toJS()));
						this[_engine].setItem(KEYS.TASKS, JSON.stringify(store.getState().tasks.keySeq().toJS()));
					}
					break;
			}

			return result;
		};
	}


	/**
	 * @returns {{
	 * 		active: Task | undefined,
	 * 		tasks: Immutable.OrderedMap<Task> | undefined
	 * }}
     */
	loadState() {
		const state = {};

		let active = this[_engine].getItem(KEYS.ACTIVE);
		if (active != null) {
			active = JSON.parse(active);

			if (active == null) {
				throw new Error('The active task has an incorrect format');
			}

			state.active = Task.create(active);
		}

		let taskList = this[_engine].getItem(KEYS.TASKS);
		if (taskList != null) {
			taskList = JSON.parse(taskList);

			if (taskList == null) {
				throw new Error('The tasks field has an incorrect format');
			}

			let tasks = Immutable.OrderedMap();

			for (const id of taskList) {
				let task = this[_engine].getItem(KEYS.task(id));
				if (task == null) {
					throw new Error(`The task was not found, id:${id}`);
				}

				task = JSON.parse(task);
				if (task == null) {
					throw new Error(`The task has an incorrect format, id:${id}`);
				}

				tasks = tasks.set(id, Task.create(task));
			}

			if (tasks.size > 0) {
				state.tasks = tasks;
			}
		}

		let sorting = this[_engine].getItem(KEYS.SORTING);
		if (sorting != null) {
			sorting = JSON.parse(sorting);

			if (sorting == null) {
				throw new Error('The sorting field has an incorrect format');
			}

			state.sorting = Sorting.create(sorting);
		}

		return state;
	}
}
