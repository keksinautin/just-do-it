import Immutable from 'immutable';
import moneyMath from 'money-math';

let lastID = 0;


/**
 * @typedef {Immutable.Map}
 */
export default class Task {
	/**
	 * @param name {string}
	 * @param cost {string}
	 * @param startTime {number}
	 * @param endTime {number}
	 * @param id {string}
	 * @returns {Task}
	 */
	static create({name = '', cost = '0.00', startTime = 0, endTime = 0, id = Task.genUID()}) {
		return Immutable.Map({name, cost, startTime, endTime, id});
	}


	/**
	 * @returns {string}
	 */
	static genUID() {
		return String(Date.now()) + ++lastID;
	}


	/**
	 * @param key {string}
	 * @param directOrder {boolean}
	 * @param a {Task|Immutable.Map}
	 * @param b {Task|Immutable.Map}
     * @returns {number}
     */
	static compare(key, directOrder, a, b) {
		if (!directOrder) {
			[b, a] = [a, b];
		}

		switch (key) {
			case 'startTime':
			case 'endTime':
				return a.get(key) - b.get(key);

			case 'cost':
				return moneyMath.cmp(a.get(key), b.get(key));

			case 'name':
				return a.get(key).localeCompare(b.get(key));
		}
	}
}
