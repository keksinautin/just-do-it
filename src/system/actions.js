export const ACTIONS = Object.freeze({
	REMOVE_TASK: 'removeTask',
	SET_ACTIVE: 'setActive',
	SET_SORTING: 'setSorting',
	SET_TASK: 'setTask'
});


export const factory = Object.freeze({
	/**
	 * @param task {Task}
	 * @returns {{type: string, payload: Task}}
     */
	[ACTIONS.REMOVE_TASK](task) {
		return {
			type: ACTIONS.REMOVE_TASK,
			payload: task
		};
	},


	/**
	 * @param task {Task}
	 * @returns {{type: string, payload: Task}}
	 */
	[ACTIONS.SET_ACTIVE](task) {
		return {
			type: ACTIONS.SET_ACTIVE,
			payload: task
		};
	},


	/**
	 * @param sorting {Sorting}
	 * @returns {{type: string, payload: Sorting}}
	 */
	[ACTIONS.SET_SORTING](sorting) {
		return {
			type: ACTIONS.SET_SORTING,
			payload: sorting
		};
	},


	/**
	 * @param task {Task}
	 * @returns {{type: string, payload: Task}}
	 */
	[ACTIONS.SET_TASK](task) {
		return {
			type: ACTIONS.SET_TASK,
			payload: task
		};
	}
});
