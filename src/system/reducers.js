import {ACTIONS} from './actions';
import Immutable from 'immutable';
import Sorting from './sorting';
import Task from './task';


export default Object.freeze({
	/**
	 * @param current {Task}
	 * @param action {{type: string, payload: Task}}
	 * @returns {Task}
     */
	active(current = Task.create({}), action = {type: '', payload: null}) {
		switch (action.type) {
			case ACTIONS.SET_ACTIVE:
				return action.payload;

			default:
				return current;
		}
	},


	/**
	 * @param current {Sorting}
	 * @param action {{type: string, payload: Sorting}}
	 * @returns {Sorting}
	 */
	sorting(current = Sorting.create({}), action = {type: '', payload: null}) {
		switch (action.type) {
			case ACTIONS.SET_SORTING:
				return action.payload;

			default:
				return current;
		}
	},


	/**
	 * @param current {Immutable.OrderedMap<string, Task>}
	 * @param action {{type: string, payload: Task}}
	 * @returns {Immutable.OrderedMap<string, Task>}
	 */
	tasks(current = Immutable.OrderedMap(), action = {type: '', payload: null}) {
		switch (action.type) {
			case ACTIONS.SET_TASK:
				//noinspection JSValidateTypes
				return current.set(action.payload.get('id'), action.payload);

			case ACTIONS.REMOVE_TASK:
				//noinspection JSValidateTypes
				return current.delete(action.payload.get('id'));

			default:
				return current;
		}
	}
});
