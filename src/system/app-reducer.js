import {ACTIONS} from './actions';
import Immutable from 'immutable';
import reducers from './reducers';
import * as Redux from 'redux';
import Task from './task';


export default () => {
	const combinedReducers = Redux.combineReducers(reducers);

	return (state, action) => {
		const newState = combinedReducers(state, action);

		switch (action.type) {
			case ACTIONS.SET_TASK:
				const sorting = newState.sorting;
				const id = action.payload.get('id');
				const prevTask = state.tasks.get(id);
				let prevSortableValue = null;
				if (prevTask) {
					prevSortableValue = prevTask.get(sorting.get('field'));
				}
				const newSortableValue = newState.tasks.get(id).get(sorting.get('field'));

				if (prevSortableValue !== newSortableValue) {
					newState.tasks = newState.tasks.sort(
						Task.compare.bind(null, sorting.get('field'), sorting.get('directOrder'))
					);
				}

				break;

			case ACTIONS.SET_SORTING:
				if (!Immutable.is(newState.sorting, state.sorting)) {
					newState.tasks = newState.tasks.sort(
						Task.compare.bind(null, newState.sorting.get('field'), newState.sorting.get('directOrder'))
					);
				}

				break;
		}

		return newState;
	};
};
