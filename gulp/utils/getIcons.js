import fs from 'fs';
import PATHS from '../paths.js';


export default function getIcons() {
	const files = fs.readdirSync(PATHS.ICONS);
	const icons = [];

	for (const file of files) {
		const result = file.match(/^([\w-]+)-(\d+x\d+)\.png$/i);

		if (result) {
			switch (result[1]) {
				case 'android-icon':
					icons.push({
						rel: 'apple-touch-icon',
						sizes: result[2],
						href: file
					});

					break;

				case 'favicon':
					icons.push({
						rel: 'icon',
						type: 'image/png',
						sizes: result[2],
						href: file
					});

					break;
			}
		}
	}

	return icons;
}
