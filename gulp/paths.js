export default {
	DIST: `${__dirname}/../dist`,
	GULP: `${__dirname}/../gulp`,
	ICONS: `${__dirname}/../src/icons`,
	JS_MAIN_SRC: `${__dirname}/../src/index.js`,
	JS_DIST_FILE: 'index.js',
	NPM: `${__dirname}/../node_modules`,
	KARMA_CONF: `${__dirname}/../karma.conf.js`,
	SRC: `${__dirname}/../src`,
	TESTS: `${__dirname}/../tests`
};
