import gulp from 'gulp';
import gulpPostCSS from 'gulp-postcss';
import PATHS from '../paths.js';
import postCssCopyAssets from 'postcss-copy-assets';


gulp.task('cp-foundation-fonts', () => {
	return gulp.src(`${PATHS.SRC}/foundation-icons/foundation-icons.css`)
		.on('error', (err) => console.error(`Error : ${err.message}`))
		.pipe(gulpPostCSS(
			[
				postCssCopyAssets({base: PATHS.DIST})
			],
			{to: PATHS.DIST})
		);
});


gulp.task('icons', () => {
	return gulp.src(`${PATHS.ICONS}/**/*`)
		.pipe(gulp.dest(PATHS.DIST));
});


gulp.task('robots', () => {
	return gulp.src(`${PATHS.SRC}/robots.txt`)
		.pipe(gulp.dest(PATHS.DIST));
});


gulp.task('resources', ['icons', 'robots', 'cp-foundation-fonts']);
