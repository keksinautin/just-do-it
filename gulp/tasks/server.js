import {argv} from 'optimist';
import browserSync from 'browser-sync';
import gulp from 'gulp';
import PATHS from '../paths.js';


gulp.task('server', () => {
	return browserSync.init({
		open: argv.open,
		browser: argv.browser,
		server: {
			baseDir: PATHS.DIST
		}
	});
});
