import del from 'del';
import gulp from 'gulp';
import PATHS from '../paths.js';


gulp.task('clear', () => del(PATHS.DIST));
