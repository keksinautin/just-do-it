import {argv} from 'optimist';
import gulp from 'gulp';
import gulpRename from 'gulp-rename';
import gulpSym from 'gulp-symlink';
import process from 'process';


const makeSymLinks = function (files, destination) {
	return gulp.src(files)
		.pipe(gulpSym(
			(file) => {
				return new gulpSym.File({
					path: destination + file.relative.replace(/\.\w+$/, '')
				});
			},
			{
				relative: true,
				force: true
			}
		));
};

const copyFiles = function (files, destination) {
	return gulp.src(files)
		.pipe(gulpRename({extname: ''}))
		.pipe(gulp.dest(destination));
};


gulp.task('set-git-hooks', () => {
	const cwd = process.cwd();

	const files = [`${cwd}/git-hooks/pre-commit.sh`, `${cwd}/git-hooks/post-merge.sh`];
	const destination = `${cwd}/.git/hooks/`;

	if (process.platform === 'win32' && !argv.useLinksForGitHooks) {
		return copyFiles(files, destination);

	}

	return makeSymLinks(files, destination);
});

