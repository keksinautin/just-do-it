import gulp from 'gulp';
import gulpWatch from 'gulp-watch';
import PATHS from '../paths.js';
import {reload} from 'browser-sync';
import runSequence from 'run-sequence';


gulp.task('watch', () => {
	gulpWatch(`${PATHS.SRC}/**/*.jade`, () => runSequence(
		'templates',
		reload
	));

	gulpWatch(`${PATHS.ICONS}/**/*`, () => runSequence(
		'icons',
		reload
	));

	gulpWatch(`${PATHS.SRC}/**/*.{js,jsx}`, () => runSequence(
		'scripts',
		reload
	));

	gulpWatch(`${PATHS.SRC}/**/*.styl`, () => runSequence(
		'styles',
		reload
	));
});
