import {argv} from 'optimist';
import gulp from 'gulp';
import gulpEsLint from 'gulp-eslint';
import gulpGitModified from 'gulp-gitmodified';
import gulpIf from 'gulp-if';
import PATHS from '../paths.js';


gulp.task('lint', () => {
	return gulp.src([
		`${PATHS.SRC}/**/*.{js,jsx}`,
		`${PATHS.GULP}/**/*.{js,jsx}`,
		`${PATHS.TESTS}/**/*.{js,jsx}`,
		`${__dirname}/gulpfile.babel.js`
	])
		.on('error', (err) => console.log(`Error : ${err.message}`))
		.pipe(gulpIf(argv.gitHook, gulpGitModified()))
		.pipe(gulpEsLint())
		.pipe(gulpEsLint.format())
		.pipe(gulpEsLint.failAfterError());
});
