import gulp from 'gulp';
import gulpCached from 'gulp-cached';
import gulpJade from 'gulp-jade';
import getIcons from '../utils/getIcons.js';
import PATHS from '../paths.js';


gulp.task('templates', () => {
	return gulp.src(`${PATHS.SRC}/**/*.jade`)
		.pipe(gulpCached('jade'))
		.pipe(gulpJade({
			data: {
				icons: getIcons(),
				timestamp: Date.now()
			}
		}))
		.pipe(gulp.dest(PATHS.DIST));
});
