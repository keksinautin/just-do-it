import {argv} from 'optimist';
import autoprefixer from 'autoprefixer';
import gulp from 'gulp';
import gulpIf from 'gulp-if';
import gulpMinifyCss from 'gulp-minify-css';
import gulpPostCSS from 'gulp-postcss';
import gulpSourceMaps from 'gulp-sourcemaps';
import gulpStylus from 'gulp-stylus';
import PATHS from '../paths.js';
import postCssImageInliner from 'postcss-image-inliner';


gulp.task('styles', () => {
	return gulp.src(`${PATHS.SRC}/index.styl`)
		.on('error', (err) => console.error(`Error : ${err.message}`))
		.pipe(gulpStylus({
			sourcemap: argv.debug ? {inline: true} : false,
			'include css': true
		}))
		.pipe(gulpIf(argv.debug, gulpSourceMaps.init()))
		.pipe(gulpPostCSS(
			[
				postCssImageInliner(
					{assetPaths: [`${PATHS.NPM}/jquery-ui/themes/base/`]}
				),
				autoprefixer
			],
			{map: false})
		)
		.pipe(gulpIf(argv.debug, gulpSourceMaps.write()))
		.pipe(gulpIf(!argv.debug, gulpMinifyCss({keepSpecialComments: 0})))
		.pipe(gulp.dest(`${PATHS.DIST}`));
});
