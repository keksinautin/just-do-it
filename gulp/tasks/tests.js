import gulp from 'gulp';
import karma from 'karma';
import PATHS from '../paths.js';


gulp.task('test', () => {
	const server = new karma.Server(
		{configFile: PATHS.KARMA_CONF}
	);

	return server.start();
});
