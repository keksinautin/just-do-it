import {argv} from 'optimist';
import browserify from 'browserify';
import gulp from 'gulp';
import gulpIf from 'gulp-if';
import gulpUglify from 'gulp-uglify';
import PATHS from '../paths.js';
import vinylBuffer from 'vinyl-buffer';
import vinylSourceStream from 'vinyl-source-stream';


gulp.task('scripts', () => {
	return browserify(PATHS.JS_MAIN_SRC, {
		debug: argv.debug,
		transform: [['envify', {global: true, NODE_ENV: argv.debug ? 'development' : 'production'}]]
	})
		.bundle()
		.on('error', (err) => console.log(`Error : ${err.message}`))
		.pipe(vinylSourceStream(PATHS.JS_DIST_FILE))
		.pipe(vinylBuffer())
		.pipe(gulpIf(!argv.debug, gulpUglify()))
		.pipe(gulp.dest(PATHS.DIST));
});
