/*global module */
module.exports = function (config) {
	config.set({
		frameworks: ['browserify', 'jasmine'],

		files: [
			'node_modules/babel-polyfill/dist/polyfill.js',
			'tests/**/*.js'
		],

		preprocessors: {
			'tests/**/*.js': ['browserify']
		},

		browserify: {
			debug: true,
			transform: ['babelify']
		},

		reporters: ['progress'],

		port: 9876,

		colors: true,

		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_INFO,

		autoWatch: false,
		singleRun: true,

		// browsers: ['Chrome']
		browsers: ['PhantomJS']
	});
};
