# Just Do It #

* Build project:
```
#!bash
npm install
```

* Test project:
```
#!bash
npm test
```

* Start project:
```
#!bash
npm start
```


## Additional params ##

* For developing use arg `--debug`
```
#!bash
gulp build --debug
```


* Start dev server with additional params:
```
#!bash
npm start -- --debug --open --browser="chromium-browser"
```
